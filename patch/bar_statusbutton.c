int
width_stbutton(Bar *bar, BarArg *a)
{
	// return TEXTW(buttonbar);
  // DO custom code to support pango markup
	return TEXTWM(buttonbar);
}

int
draw_stbutton(Bar *bar, BarArg *a)
{
	// return drw_text(drw, a->x, a->y, a->w, a->h, lrpad / 2, buttonbar, 0, False);
  // DO custom code to support pango markup
	return drw_text(drw, a->x, a->y, a->w, a->h, lrpad / 2, buttonbar, 0, True);
}

int
click_stbutton(Bar *bar, Arg *arg, BarArg *a)
{
	return ClkButton;
}

